drop DATABASE if EXISTS examen1702;
create database examen1702;

use examen1702;

create table movies(
    id integer AUTO_INCREMENT PRIMARY KEY,
    title varchar(100),
    director varchar(100),
    year int    
    );
    
INSERT INTO movies VALUES
    (1, "Casablanca", "Michael Curtiz", 1942),
    (2, "Cinama Paradiso", "Giuseppe Tornatore", 1998),
    (3, "Sin Perdón", "Clint Eastwood", 1992);