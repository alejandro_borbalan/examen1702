package dbconnect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;



public class mysqlDAO {

    public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://localhost/examen1702";
    public static final String DB_USER = "root";
    public static final String DB_PASSWORD = "root";
    public Connection connection = null;
    
    public mysqlDAO() {
        try {
            Class.forName(DB_DRIVER).newInstance();
        } catch (Exception ex) {
            System.out.println("no se ha cargado el driver");
        }
        try {
            connection = DriverManager.getConnection(DB_URL + "?user=" + DB_USER + "&password=" + DB_USER);
            System.out.println("Conectado");

        } catch (SQLException ex) {
            System.out.println("Fallo Conexion");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (connection != null) {
            connection.close();        
        }
    }
}